package com.kamil.SpringAcademyLearning.controller;

import com.kamil.SpringAcademyLearning.repository.Car;
import com.kamil.SpringAcademyLearning.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CarController {

    CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping
    public List<Car>getCarList(){
        return carService.getCarList();
    }
}

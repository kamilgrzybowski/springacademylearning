package com.kamil.SpringAcademyLearning.repository;

import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public class Car {
    private String mark;
    private String model;
    private LocalDate productionDate;
    private Engine engine;

    public Car() {
    }

    public Car(String mark, String model, LocalDate productionDate, Engine engine) {
        this.mark = mark;
        this.model = model;
        this.productionDate = productionDate;
        this.engine = engine;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(LocalDate productionDate) {
        this.productionDate = productionDate;
    }

    @Override
    public String toString() {
        return "Car{" +
                "mark='" + mark + '\'' +
                ", model='" + model + '\'' +
                ", productionDate=" + productionDate +
                ", engine=" + engine +
                '}';
    }
}

package com.kamil.SpringAcademyLearning.service;


import com.kamil.SpringAcademyLearning.repository.Car;
import com.kamil.SpringAcademyLearning.repository.Engine;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class CarService {

    private List<Car> carList = new ArrayList<>();

    public CarService(){
        Car car = new Car("BMW","E46", LocalDate.of(2001,01,01), Engine.V8);
        Car car1 = new Car("Fiat","126p", LocalDate.of(1972,01,01), Engine.V8);
        Car car2 = new Car("Mercedes","AMG S65", LocalDate.of(2015,01,01), Engine.V12);

        carList.add(car);
        carList.add(car1);
        carList.add(car2);
    }

    public List<Car> getCarList(){
         return carList;
        }
}

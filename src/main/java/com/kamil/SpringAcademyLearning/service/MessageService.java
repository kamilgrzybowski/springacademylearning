package com.kamil.SpringAcademyLearning.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public class MessageService {
    private MessageSource messageSource;

    @Autowired
    public MessageService(MessageSource messageSource) {
        this.messageSource = messageSource;

        String textDE = messageSource.getMessage("hello",new Object[]{"Kamil","Grzybowski"}, Locale.forLanguageTag("de"));
        String textPL = messageSource.getMessage("hello",new Object[]{"Kamil","Grzybowski"}, Locale.forLanguageTag("pl"));

        System.out.println("German: " + textDE);
        System.out.println("Polish: " + textPL);
    }
}
